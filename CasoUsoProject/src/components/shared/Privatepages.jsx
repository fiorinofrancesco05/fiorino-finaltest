import Cookies from '../../../node_modules/js-cookie'
import { Outlet, Navigate } from 'react-router-dom'

export default function Privatepages(){

    if(Cookies.get("email") != undefined){
        return(
            <Outlet />
        )
    }

    return(
        <>
        <Navigate to="/accesso" />
        </>
    )
}