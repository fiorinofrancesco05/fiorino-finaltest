import { useState, useContext } from "react";
import { UserContext } from "../../context/UserContext";
import { Link, NavLink } from "react-router-dom"
import { useNavigate } from "react-router-dom";
import css from "../cssLink.module.css"

export default function Registration() {

    //importazione per uso contesto
    const [userData, setUserData, registrato, setRegistrato] = useContext(UserContext)

    //modello form
    const [formData, setFormData] = useState({
        nome: "",
        cognome: "",
        email: "",
        password: "",
    })

    //man mano che si aggiorna il form si aggiornano le variabili
    function handleChange(e) {
        const { name, value } = e.target;
        setFormData({ ...formData, [name]: value });
    }

    //validazione dati
    function handleSubmit(e) {
        e.preventDefault();


        if (formData.nome.match("[a-zA-Z\\èàòìù]{1,255}") && formData.cognome.match("[a-zA-Z\\èàòìù]{1,255}")
            && formData.email.match("[A-z0-9\\.\\+_-]+@[A-z0-9\\._-]+\\.[A-z]{2,20}")
            && formData.password.match("(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@$!%*?&])[A-Za-z\\d@$!%*?&]{6,20}")) {

            fetchData();

        } else {

            alert("insersci esattamente le regole")

        }



    }

    //richiesta POST per la registrazione
    async function fetchData() {

        const response = await fetch("http://localhost:8080/corso/user/registration",
            {
                headers: {
                    'Content-Type': 'application/json',
                },
                method: "POST",
                body: JSON.stringify(formData)

            })


        setRegistrato(true)

    }

    //nel caso sia già registrato
    function menageRegistration() {

        setRegistrato(true);


    }


    return (
        <>
            <div className="container-fluid">

                <div className="row mt-5">

                    <div className="col-md-4 mt-5"></div>

                    <div style={{ border: "1px solid white", borderRadius: "8px" }} className="col-md-4 mt-5 bg-dark">
                        <form onSubmit={handleSubmit}>

                            <div className="mt-3 text-center">
                                <svg style={{fill:"white"}} xmlns="http://www.w3.org/2000/svg" width="56" height="56" fill="currentColor" class="bi bi-person-circle" viewBox="0 0 16 16">
                                    <path d="M11 6a3 3 0 1 1-6 0 3 3 0 0 1 6 0" />
                                    <path fill-rule="evenodd" d="M0 8a8 8 0 1 1 16 0A8 8 0 0 1 0 8m8-7a7 7 0 0 0-5.468 11.37C3.242 11.226 4.805 10 8 10s4.757 1.225 5.468 2.37A7 7 0 0 0 8 1" />
                                </svg>
                                <h1 style={{ color: "white" }}>Registrati</h1>
                            </div>

                            <div className="form-floating mb-3 mt-4">
                                <input type="text" className="form-control" name="nome" id="floatingInput" placeholder="Inserire Email" onChange={handleChange} value={formData.nome} autoComplete="off" />
                                <label htmlFor="floatingInput">Nome</label>
                            </div>

                            <div className="form-floating mb-3">
                                <input type="text" className="form-control" name="cognome" id="floatingInput2" placeholder="Inserire Email" onChange={handleChange} value={formData.cognome} autoComplete="off" />
                                <label htmlFor="floatingInput">Cognome</label>
                            </div>

                            <div className="form-floating mb-3">
                                <input type="email" className="form-control" name="email" id="floatingInput3" placeholder="Inserire Email" onChange={handleChange} value={formData.email} autoComplete="off" />
                                <label htmlFor="floatingInput">Email</label>
                            </div>

                            <div className="form-floating mb-3">
                                <input type="password" name="password" className="form-control" id="floatingPassword" placeholder="Password" onChange={handleChange} value={formData.password} autoComplete="off" />
                                <label htmlFor="floatingPassword">Password</label>
                            </div>

                            <div style={{ color: "white" }} >
                                <ul>
                                    <li>La password deve essere di una lunghezza tra 6 e 20 caratteri.</li>
                                    <li>Deve contenere almeno lettere minuscole, lettere maiuscole, un numero e un carattere speciale</li>
                                </ul>
                            </div>

                            <div className="text-center">
                            <input type="submit" className="btn btn-success mt-3 mb-2 " value="Iscriviti" />

                            </div>
                        </form>

                        <p className="mt-2 mb-2 text-center" style={{ color: "white" }}>
                            <span >Sei già registrato? Clicca </span>
                            <span onClick={menageRegistration} className={css.myLink}>qui</span>
                            
                          <span> per accedere</span></p>
                    </div>



                    <div className="col-md-4"></div>

                </div>
            </div>



        </>
    )
}