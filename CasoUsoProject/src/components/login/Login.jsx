
import { useState, useContext } from "react";
import { UserContext } from "../../context/UserContext";
import { useNavigate } from "react-router-dom";
import Cookies from '../../../node_modules/js-cookie'
import { jwtDecode } from "../../../node_modules/jwt-decode";
import css from "../cssLink.module.css"



export default function Login() {

    //contesto importato
    const [userData, setUserData, registrato, setRegistrato] = useContext(UserContext)

    //modello form
    const [formData, setFormData] = useState({
        email: "",
        password: "",
    })

    //navigate
    const navigateTo = useNavigate();

    //menage dei dati nel form
    function handleChange(e) {
        const { name, value } = e.target;
        setFormData({ ...formData, [name]: value });
    }

    //validazione dati
    function handleSubmit(e) {
        e.preventDefault();


        if (formData.email.match("[A-z0-9\\.\\+_-]+@[A-z0-9\\._-]+\\.[A-z]{2,20}")
            && formData.password.match("(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@$!%*?&])[A-Za-z\\d@$!%*?&]{6,20}")) {

            fetchData();

        } else {

            alert("insersci esattamente i dati")

        }

    }

    //richiesta POST per l'accesso
    async function fetchData() {

        const response = await fetch("http://localhost:8080/corso/user/login",
            {
                headers: {
                    'Content-Type': 'application/json',
                },
                method: "POST",
                body: JSON.stringify(formData)

            })

        if(response.status == 400){
            alert("Errore, riprova l'inserimento dei dati")

        }else{


        //prendo la response dal server
        const tokenResponse = await response.json();

        //prendo il token
        const token = tokenResponse.token;

        Cookies.set("token", token)

        Cookies.set("ttl", tokenResponse.ttl)

        //decodifica token
        const decoded = jwtDecode(token);

        const ruoli = decoded.Ruoli
        const nome = decoded.nome
        const cognome = decoded.cognome
        const email = decoded.email
       

        setUserData({...userData, "nome": nome, "cognome": cognome, "email":email});
    
        //inserimento token nei cookies

        //var inFifteenMinutes = new Date(new Date().getTime() + 15 * 60 * 1000); 
        //COMMENTATO PER UNA QUESTIONE DI PRATICITA'

        let inOneYear = 365

        Cookies.set("nome", nome, {
            expires: inOneYear
        })

        Cookies.set("cognome", cognome, {
            expires: inOneYear
        })

        Cookies.set("email", email, {
            expires: inOneYear
        })
        
        Cookies.set("ruoli", ruoli, {
            expires: inOneYear
        })

        navigateTo("/")
        }


        


    }

    //cambio variabile per accesso o registrazione
    function menageAccesso(){
    
        setRegistrato(false);
       
    }



    return (
        <>
            <div className="container-fluid text-center">

                <div className="row mt-5">

                    <div className="col-md-4 mt-5"></div>

                    <div style={{ border: "1px solid white", borderRadius: "8px" }} className="col-md-4 mt-5 bg-dark">
                        <form onSubmit={handleSubmit}>

                        <div className="mt-3 text-center">
                                <svg style={{fill:"white"}} xmlns="http://www.w3.org/2000/svg" width="56" height="56" fill="currentColor" class="bi bi-person-circle" viewBox="0 0 16 16">
                                    <path d="M11 6a3 3 0 1 1-6 0 3 3 0 0 1 6 0" />
                                    <path fill-rule="evenodd" d="M0 8a8 8 0 1 1 16 0A8 8 0 0 1 0 8m8-7a7 7 0 0 0-5.468 11.37C3.242 11.226 4.805 10 8 10s4.757 1.225 5.468 2.37A7 7 0 0 0 8 1" />
                                </svg>
                                <h1 style={{ color: "white" }}>Accedi</h1>
                            </div>

                            <div className="form-floating mb-3 mt-3">
                                <input type="email" className="form-control" name="email" id="floatingInput3" placeholder="Inserire Email" onChange={handleChange} value={formData.email} />
                                <label htmlFor="floatingInput">Email</label>
                            </div>

                            <div className="form-floating mb-3">
                                <input type="password" name="password" className="form-control" id="floatingPassword" placeholder="Password" onChange={handleChange} value={formData.password} />
                                <label htmlFor="floatingPassword">Password</label>
                            </div>

                            <input type="submit" className="btn btn-success mt-3 mb-2" value="Accedi" />
                        </form>

                        <p className="mt-2 mb-2" style={{ color: "white" }}>
                            <span >Non ti sei registrato? Clicca </span>
                            <span onClick={menageAccesso} className={css.myLink}>qui</span> 
                            <span> per registrarti!</span></p>

                    </div>

                    <div className="col-md-4"></div>

                </div>

                
            </div>



        </>
    )
}