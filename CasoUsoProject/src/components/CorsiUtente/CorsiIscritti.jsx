import Cookies from "js-cookie"
import { useState, useEffect } from "react"
import CardCorsoUtente from "../Card/CardCorsoUtente";

export default function CorsiIscritti(){

    const [corsiData, setCorsiData] = useState([])

    useEffect(() => {

        getCorsi()

    }, []);

    async function getCorsi(){

        const email = {
            email: Cookies.get("email")
        }

        const response = await fetch("http://localhost:8080/corso/corsi/getcorsiiscritti",{
            headers: {
                'Content-Type': 'application/json'
            },
            method: "POST",
            body: JSON.stringify(email)
        })

        const dataCard = await response.json();

        if(response.status == 200){
            setCorsiData(
                dataCard.map((corso) => ({
                    nomeCorso: corso.nomeCorso,
                    nomeCategoria: corso.categoria.nome_categoria,
                    descrizione_Breve: corso.descrizioneBreve,
                    descrizione_Completa: corso.descrizioneCompleta,
                    id: corso.id,
                    durata: corso.durata,
                }))
            )
        }


    }

    return(
        <>
        <div className="mt-t">
            <div>

            {corsiData.map((corso) => (

            <CardCorsoUtente key={corso.id} idCorso={corso.id} nomeCorso={corso.nomeCorso} nomeCategoria={corso.nomeCategoria}
            descrizione_Breve={corso.descrizione_Breve} descrizione_Completa={corso.descrizione_Completa} durata={corso.durata}
            updateCard={getCorsi}>
            </CardCorsoUtente>

            ))}
                

            </div>
        </div>
        </>
    )
}