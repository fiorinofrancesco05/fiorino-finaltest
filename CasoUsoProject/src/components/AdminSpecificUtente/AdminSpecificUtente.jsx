import { useContext, useEffect, useState } from "react";
import { UserContext } from "../../context/UserContext";
import css from "./SpecificUtenteTable.module.css";
import { useNavigate } from "react-router-dom";
import Cookies from 'js-cookie'

export default function AdminSpecificUtente() {

  //dati dell'utente selezionato tramite grafica
  const [profilo, setProfilo] = useState({
    nome: "",
    cognome: "",
    email: ""
  })

  useEffect(() => {

    fetchUser()

  }, [])



  const navigateTo = useNavigate()


  // Menage dei dati nel form
  function handleChange(e) {

    const { name, value } = e.target;
    setProfilo({ ...profilo, [name]: value });


  }


  //invio e validazione dati form
  function handleSubmit(e) {
    e.preventDefault();

    if (profilo.nome.match("[a-zA-Z\\èàòìù]{1,255}") && profilo.cognome.match("[a-zA-Z\\èàòìù]{1,255}")
      && profilo.email.match("[A-z0-9\\.\\+_-]+@[A-z0-9\\._-]+\\.[A-z]{2,20}")) {

      fetchData();

    } else {
      alert("Dati non validi")
    }

  }


  //recupero dati utente
  async function fetchUser() {

    const emailUser = {
      email: Cookies.get("emailModificaAdmin")
    }

    const response = await fetch("http://localhost:8080/corso/user/getuserbyemail", {
      method: "POST",
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(emailUser)
    })

    const dati = await response.json();


    setProfilo({
      nome: dati.nome,
      cognome: dati.cognome,
      email: dati.email
    });


  }




  //invio modifica dati utente
  async function fetchData() {

    const dataUpdate = { nome: profilo.nome, cognome: profilo.cognome, newEmail: profilo.email, oldEmail: Cookies.get("emailModificaAdmin") }

    const response = await fetch("http://localhost:8080/corso/user/updateuser",
      {
        headers: {
          'Content-Type': 'application/json',
        },
        method: "PUT",
        body: JSON.stringify(dataUpdate)

      })

    if (response.status == 200) {
      alert("modifica effettuata");
      navigateTo("/allutenti")
    }




  }

  return (
    <>
      <h1 className="mt-5 mb-5 container-fluid text-center">Admin only: Modifica utente</h1>


      <div className="container text-center">

        <form onSubmit={handleSubmit}>

          {/* riga contenente la tabella */}
          <div className="row">
            <div className="col-sm-2"></div>

            <div className="col-sm-8">
              <div className="table-responsive">
                <table className="table table-dark table-striped table-hover">
                  <thead>
                    <tr>
                      <th scope="col">#</th>
                      <th scope="col">Nome</th>
                      <th scope="col">Cognome</th>
                      <th scope="col">Email</th>
                    </tr>
                  </thead>
                  <tbody>

                    <tr>
                      <th scope="row" className="text-center align-middle">
                        {1}
                      </th>
                      <td className="text-center align-middle">
                        <input className={css.InputTable} name="nome" value={profilo.nome} onChange={handleChange} />
                      </td>
                      <td className="text-center align-middle">
                        <input className={css.InputTable} name="cognome" value={profilo.cognome} onChange={handleChange} />
                      </td>
                      <td className="text-center align-middle">
                        <input className={css.InputTable} name="email" value={profilo.email} onChange={handleChange} />
                      </td>

                    </tr>

                  </tbody>
                </table>
              </div>
            </div>

            <div className="col-sm-2"></div>
          </div>{/* fine riga */}

          <input type="submit" className="btn btn-success mt-3 mb-2" value="Modifica" />
        </form>


        <div className="container text-start mt-5" style={{ width: "75%" }}>
          <h3>Istruzioni per l'uso:</h3>
          <ul style={{fontSize:"1.2em"}}>
            <li>Per modificare i dati cambiare il contenuto nelle caselle di input</li>
            <li>Una volta terminato, e cliccato il bottone, le modifiche saranno permanenti</li>
          </ul>     
        </div>

      </div>

    </>
  );
}
