import css from "../Card/CardCorso.module.css"
import Cookies from 'js-cookie'
import copertina from "../../images/informatica.jpg"

export default function CardCorsoUtente({ idCorso = "", nomeCorso = "", nomeCategoria = "", descrizione_Breve = "", descrizione_Completa = "", durata = "", updateCard }) {


    
    //controllo della stringa
    function controlloStringa(stringa) {

        if (stringa === undefined) stringa = ""

        if (stringa != null && stringa != undefined && stringa != "") {
            return true
        } else {
            return false
        }
    }

    async function unsubscribeCorso(){


        const emailUtente = Cookies.get("email");
    
        const requestUrl = `http://localhost:8080/corso/user/${idCorso}/deleteutente-corso/${emailUtente}`;

        const response = await fetch(requestUrl, {
            headers: {
                'Content-Type': 'application/json'
            },
            method: "PUT",
        })

        if(response.status == 200){
            alert("Unsubscribe effettuata!")
            updateCard()
        }else(
            alert("Errore durante l'unsubscribe, riprova!")
        )


    }
        
    
    return (

        <>

            <div className="row">

                <div className="col-md-4"></div>

                <div className="col-md-4 mb-4">

                    <div className={`card ${css.CardBorder}`}>
                        <div className="card-header bg-success" >
                        <img src={copertina}/>
                        </div>
                        <div className="card-body">
                            <p className="card-text">
                                <h5>{nomeCorso} </h5>
                            {descrizione_Breve}</p>

                            <button type="button" className="btn btn-primary" data-bs-toggle="modal" data-bs-target={`#${idCorso}`}>
                                Visualizza dettagli
                            </button>

                

                            <div className="modal fade" id={idCorso} data-bs-backdrop="static" data-bs-keyboard="false" tabIndex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                                <div className="modal-dialog">
                                    <div className="modal-content">
                                        <div className="modal-header">
                                            <h1 className="modal-title fs-5" id="staticBackdropLabel">{nomeCorso}</h1>
                                            <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                        </div>
                                        <div className="modal-body">
                                            <p className="text-start">Descrizione: {descrizione_Completa}</p>
                                            <p className="text-start">Categoria corso: {nomeCategoria}</p>
                                            <p className="text-start">Durata corso: {durata} mesi</p>

                                        </div>
                                        <div className="modal-footer">
                                         
                                        {controlloStringa(Cookies.get("email")) ? (
                                            <button type="button" className="btn btn-danger" onClick={() => unsubscribeCorso()} data-bs-dismiss="modal">Disiscriviti dal corso</button>
                                            ) : null}

                                            <button type="button" className="btn btn-secondary" data-bs-dismiss="modal">Chiudi</button>

                                        </div>
                                    </div>
                                </div>
                            </div>



                        </div>
                    </div>
                </div>

                <div className="col-md-4"></div>
            </div>


        </>

    )

}