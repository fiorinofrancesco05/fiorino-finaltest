import css from "../Card/CardCorso.module.css"
import Cookies from 'js-cookie'
import copertina from "../../images/informatica.jpg"
import { useNavigate } from "react-router-dom";

export default function CardCorso({ idCorso = "", nomeCorso = "", nomeCategoria = "", descrizione_Breve = "", descrizione_Completa = "", durata = "", updateCard }) {


    const navigateTo = useNavigate();
    
    //controllo della stringa
    function controlloStringa(stringa) {

        if (stringa === undefined) stringa = ""

        if (stringa != null && stringa != undefined && stringa != "") {
            return true
        } else {
            return false
        }
    }

    //controlla nei cookies se è presente il ruolo admin
    function controlloRuoloAdmin(ruoli) {

        let trovato = false;

        if (ruoli === undefined) ruoli = ""

        if (ruoli.includes("admin") || ruoli.includes("Admin")) trovato = true;

        return trovato
    }

    //gestione delete corso
    async function handledelete(id){

        let token = Cookies.get("token")
        const idCorso = {
            id:id
        }

        const response = await fetch("http://localhost:8080/corso/corsi/deletecorso",{
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            method: "POST",
            body: JSON.stringify(idCorso)
        })

        if(response.status == 200){
            alert("Cancellazione effettuata")
            updateCard()
        }
            
    }

    //gestione modifica corso
    async function handleUpdateCorso(idCorso){

       Cookies.set("idCorsoUpdate", idCorso);
       navigateTo("/updatecorsoadmin")
            
    }

    //iscrizione al corso
    async function iscrizioneCorso(){

        const emailUtente = Cookies.get("email");
    
        const requestUrl = `http://localhost:8080/corso/user/${idCorso}/addUtenteToCorso/${emailUtente}`;

        const response = await fetch(requestUrl, {
            method: "PUT"
        });

        if(response.status == 200){
            alert("Iscrizione effettuata!")
        }else(
            alert("Sei già iscritto/ Errore durante l'iscrizione!")
        )
        

    }

    return (

        <>

            <div className="row">

                <div className="col-md-4"></div>

                <div className="col-md-4 mb-4">

                    <div className={`card ${css.CardBorder}`}>
                        <div className="card-header bg-success" >
                        <img src={copertina}/>
                        </div>
                        <div className="card-body">
                            <p className="card-text">
                                <h5>{nomeCorso} </h5>
                            {descrizione_Breve}</p>

                            {controlloRuoloAdmin(Cookies.get("ruoli")) ? (
                                <button type="button" className="btn btn-danger me-2" data-bs-dismiss="modal" onClick={() => handleUpdateCorso(idCorso)}>Modifica corso</button>
                            ) : null}

                            <button type="button" className="btn btn-primary" data-bs-toggle="modal" data-bs-target={`#${idCorso}`}>
                                Visualizza dettagli
                            </button>

                            {controlloRuoloAdmin(Cookies.get("ruoli")) ? (
                                <button type="button" className="btn btn-danger ms-2" data-bs-dismiss="modal" onClick={() => handledelete(idCorso)}>Elimina corso</button>
                            ) : null}

                            <div className="modal fade" id={idCorso} data-bs-backdrop="static" data-bs-keyboard="false" tabIndex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                                <div className="modal-dialog">
                                    <div className="modal-content">
                                        <div className="modal-header">
                                            <h1 className="modal-title fs-5" id="staticBackdropLabel">{nomeCorso}</h1>
                                            <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                        </div>
                                        <div className="modal-body">
                                            <p className="text-start">Descrizione: {descrizione_Completa}</p>
                                            <p className="text-start">Categoria corso: {nomeCategoria}</p>
                                            <p className="text-start">Durata corso: {durata} mesi</p>

                                        </div>
                                        <div className="modal-footer">
                                         
                                        {controlloStringa(Cookies.get("email")) ? (
                                            <button type="button" className="btn btn-success" onClick={() => iscrizioneCorso()} data-bs-dismiss="modal">Iscriviti al corso</button>
                                            ) : null}

                                            <button type="button" className="btn btn-secondary" data-bs-dismiss="modal">Chiudi</button>

                                        </div>
                                    </div>
                                </div>
                            </div>



                        </div>
                    </div>
                </div>

                <div className="col-md-4"></div>
            </div>


        </>

    )

}