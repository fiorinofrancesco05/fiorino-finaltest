import { useContext, useEffect, useState } from "react";
import { UserContext } from "../../context/UserContext";
import css from "../AdminSpecificUtente/SpecificUtenteTable.module.css";
import { useNavigate } from "react-router-dom";
import Cookies from '../../../node_modules/js-cookie'

export default function ProfileTable() {

  //dati dell'utente loggato
  const [profilo, setProfilo] = useState({
    nome: "",
    cognome: "",
    email: ""
  })


  useEffect(() => {

    fetchUser()


  }, [])



  const navigateTo = useNavigate()

  // Menage dei dati nel form
  function handleChange(e) {

    const { name, value } = e.target;
    setProfilo({ ...profilo, [name]: value });

  }

  //invio e validazione form
  function handleSubmit(e) {
    e.preventDefault();

    if (profilo.nome.match("[a-zA-Z\\èàòìù]{1,255}") && profilo.cognome.match("[a-zA-Z\\èàòìù]{1,255}")
      && profilo.email.match("[A-z0-9\\.\\+_-]+@[A-z0-9\\._-]+\\.[A-z]{2,20}")) {

      fetchData();

    } else {
      alert("Dati non validi")
    }

  }



  //recupero dati utente tramite vecchia email
  async function fetchUser() {

    const emailUser = {
      email: Cookies.get("email")
    }

    const response = await fetch("http://localhost:8080/corso/user/getuserbyemail", {
      method: "POST",
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(emailUser)
    })

    const dati = await response.json();


    setProfilo({
      nome: dati.nome,
      cognome: dati.cognome,
      email: dati.email
    });


  }


  //modifica dati utente
  async function fetchData() {

    const dataUpdate = { nome: profilo.nome, cognome: profilo.cognome, newEmail: profilo.email, oldEmail: Cookies.get("email") }

    const response = await fetch("http://localhost:8080/corso/user/updateuser",
      {
        headers: {
          'Content-Type': 'application/json',
        },
        method: "PUT",
        body: JSON.stringify(dataUpdate)

      })

    if (response.status == 200) {
      alert("modifica effettuata");
      navigateTo("/")
    }

  }


  return (
    <>
      <h1 className="mt-5 mb-5 container-fluid text-center">Modifica i tuoi dati</h1>


      <div className="container text-center">

        <form onSubmit={handleSubmit}>

          {/* riga contenente la tabella */}
          <div className="row">
            <div className="col-md-4"></div>

            <div style={{ border: "1px solid white", borderRadius: "8px" }} className="col-md-4 bg-dark">
              <div className="mt-3 text-center">
                <svg style={{ fill: "white" }} xmlns="http://www.w3.org/2000/svg" width="56" height="56" fill="currentColor" class="bi bi-person-circle" viewBox="0 0 16 16">
                  <path d="M11 6a3 3 0 1 1-6 0 3 3 0 0 1 6 0" />
                  <path fill-rule="evenodd" d="M0 8a8 8 0 1 1 16 0A8 8 0 0 1 0 8m8-7a7 7 0 0 0-5.468 11.37C3.242 11.226 4.805 10 8 10s4.757 1.225 5.468 2.37A7 7 0 0 0 8 1" />
                </svg>
                <h1 style={{ color: "white" }}>Profilo</h1>
              </div>

              <div className="form-floating mb-3 mt-4">
                <input type="text" className="form-control" name="nome" id="floatingInput" placeholder="Inserire Email" onChange={handleChange} value={profilo.nome} autoComplete="off" />
                <label htmlFor="floatingInput">Nome</label>
              </div>

              <div className="form-floating mb-3">
                <input type="text" className="form-control" name="cognome" id="floatingInput2" placeholder="Inserire Email" onChange={handleChange} value={profilo.cognome} autoComplete="off" />
                <label htmlFor="floatingInput">Cognome</label>
              </div>

              <div className="form-floating mb-3">
                <input type="email" className="form-control" name="email" id="floatingInput3" placeholder="Inserire Email" onChange={handleChange} value={profilo.email} autoComplete="off" />
                <label htmlFor="floatingInput">Email</label>
              </div>


              <div className="text-center">
                <input type="submit" className="btn btn-success mt-3 mb-2 " value="Modifica" />

              </div>
            </div>

            <div className="col-md-4"></div>
          </div>{/* fine riga */}


        </form>
      </div>

    </>
  );
}
