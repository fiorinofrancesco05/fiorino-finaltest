
import { Link, NavLink } from "react-router-dom"
import { UserContext } from "../../context/UserContext"
import { useContext } from "react"
import Cookies from 'js-cookie'
import { useNavigate } from "react-router-dom";


export default function Navbar() {

    //contesto importato
    const [userData, setUserData, registrato, setRegistrato] = useContext(UserContext)

    const navigate = useNavigate()

    //controllo della stringa
    function controlloStringa(stringa) {

        if (stringa === undefined) stringa = ""

        if (stringa != null && stringa != undefined && stringa != "") {
            return true
        } else {
            return false
        }
    }


    //controllo il ruolo utente
    function controlloRuoloAdmin(ruoli) {

        let trovato = false;

        if (ruoli === undefined) ruoli = ""

        if (ruoli.includes("admin") || ruoli.includes("Admin")) trovato = true;

        return trovato
    }

    //funzione per pulizia dati successiva al logout
    function handleLogOut() {

        Cookies.remove("nome")
        Cookies.remove("cognome")
        Cookies.remove("email")
        Cookies.remove("ruoli")
        Cookies.remove("emailModificaAdmin")
        Cookies.remove("token")

        setUserData({})
        setRegistrato(false);

        navigate("/")

    }


    return (

        <>

            <nav style={{backgroundColor: "green"}} className="navbar navbar-expand-lg navbar-dark">
                <div className="container-fluid">
                    <span className="navbar-brand">
                        Accademy SI
                    </span>
                    <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav"
                        aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"></span>
                    </button>
                    <div className="collapse navbar-collapse justify-content-end" id="navbarNav">
                        <ul className="navbar-nav">
                            
                            {controlloStringa(Cookies.get("email")) ? (
                                <li className="nav-item">
                                    <Link className="nav-link active" onClick={handleLogOut}>- Logout - </Link>
                                </li>
                            ) : (<li className="nav-item">
                                <Link className="nav-link active" to="/accesso">- Accesso - </Link>
                            </li>)}
                            <Link className="nav-link active" to="/">
                                Home
                            </Link>



                            {controlloStringa(Cookies.get("email")) ? (
                                <li className="nav-item">
                                    <Link className="nav-link active" to="/profilo">Profilo</Link>
                                </li>
                            ) : null}

                            <li className="nav-item">
                                <Link className="nav-link active" to="/corsi">Corsi</Link>
                            </li>

                            {controlloRuoloAdmin(Cookies.get("ruoli")) ? (
                                <li className="nav-item">
                                    <Link className="nav-link active" to="/allutenti">Totale Utenti</Link>
                                </li>) : null}
                        </ul>
                    </div>
                </div>
            </nav>

        </>

    )

}