import { useContext, useEffect, useState } from "react"
import { UserContext } from "../../context/UserContext"
import { useNavigate } from "react-router-dom"
import Cookies from '../../../node_modules/js-cookie'

export default function TabellaUtenti() {

    const navigateTo = useNavigate();

    //dati utenti
    const [utenti, setUtenti] = useState([])

    //email per eliminazione
    const [emailRow,setEmail] = useState({})

    //richiesta dati utente
    async function fetchData() {

        let token = Cookies.get("token")

        const response = await fetch("http://localhost:8080/corso/user/printAll", {
            method: "GET",
            headers:{
                'Authorization': `Bearer ${token}`
            }
        })

        let dataUser = await response.json();

        setUtenti(
            dataUser.map((utente) => ({
                nome: utente.nome,
                cognome: utente.cognome,
                email: utente.email,
                ruoli: utente.ruoli.map((ruolo) => ruolo.tipologia).join(", ")

            }))
        );

    }

    //richiamo al caricamento il server per i dati
    useEffect(() => {

        fetchData()

    }, [])


    //cancellazione utente
    async function deleteUser(email){

        setEmail({...emailRow, "email": email})

        let token = Cookies.get("token")

        const response = await fetch("http://localhost:8080/corso/user/deleteuser",{
            method:"POST",
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            body: JSON.stringify(emailRow)
        })

        if(response.status == 200){
            alert("Utente cancellato con successo")
            fetchData()
        }
        
    }

    //cambio di pagina per modifica user
    function modifyUser(utente){

    Cookies.set("emailModificaAdmin", utente.email)

    navigateTo("/specificutente")
    
    }

    return (
        <>

            <h1 className="mt-5 mb-5">Admin only: Tabella completa utenti</h1>

            <div className="container">
                <table className="table table-dark table-striped table-hover">

                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Nome</th>
                            <th scope="col">Cognome</th>
                            <th scope="col">Email</th>
                            <th scope="col">Ruoli</th>
                            <th scope="col">Opzioni</th>
                        </tr>
                    </thead>
                    <tbody>

                        {utenti.map((utente, index) => (
                            <tr key={++index}>
                                <th scope="row">{++index}</th>
                                <td>{utente.nome}</td>
                                <td>{utente.cognome}</td>
                                <td>{utente.email}</td>
                                <td>{utente.ruoli ? utente.ruoli : "Nessun ruolo"}</td>
                                <td>

                                    <svg onClick={() => deleteUser(utente.email)} style={{ fill: "white" }} xmlns="http://www.w3.org/2000/svg" width="18" height="18" fill="currentColor" className="bi bi-trash3" viewBox="0 0 16 16">
                                        <path d="M6.5 1h3a.5.5 0 0 1 .5.5v1H6v-1a.5.5 0 0 1 .5-.5M11 2.5v-1A1.5 1.5 0 0 0 9.5 0h-3A1.5 1.5 0 0 0 5 1.5v1H1.5a.5.5 0 0 0 0 1h.538l.853 10.66A2 2 0 0 0 4.885 16h6.23a2 2 0 0 0 1.994-1.84l.853-10.66h.538a.5.5 0 0 0 0-1zm1.958 1-.846 10.58a1 1 0 0 1-.997.92h-6.23a1 1 0 0 1-.997-.92L3.042 3.5zm-7.487 1a.5.5 0 0 1 .528.47l.5 8.5a.5.5 0 0 1-.998.06L5 5.03a.5.5 0 0 1 .47-.53Zm5.058 0a.5.5 0 0 1 .47.53l-.5 8.5a.5.5 0 1 1-.998-.06l.5-8.5a.5.5 0 0 1 .528-.47M8 4.5a.5.5 0 0 1 .5.5v8.5a.5.5 0 0 1-1 0V5a.5.5 0 0 1 .5-.5" />
                                    </svg>

                                    <svg onClick={() => modifyUser(utente)} style={{ fill: "white" }} xmlns="http://www.w3.org/2000/svg" width="18" height="18" fill="currentColor" className="bi bi-pencil-square ms-2" viewBox="0 0 16 16">
                                        <path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z" />
                                        <path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5z" />
                                    </svg>
                                </td>
                            </tr>
                        ))}

                    </tbody>
                </table>
            </div>
        </>
    )
}