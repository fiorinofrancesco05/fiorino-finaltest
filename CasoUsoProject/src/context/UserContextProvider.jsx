import { useState } from "react";
import {UserContext} from "../context/UserContext"

export default function UserContextProvider({children}){

    const [userData, setUserData] = useState({
        nome:"",
        cognome:"",
        email:"",
        ruoli:""
    });

    const [registrato, setRegistrato] = useState(false)
    
    return(
    
        <UserContext.Provider value={[userData, setUserData, registrato, setRegistrato]}>
            {children}
        </UserContext.Provider>
    
    )
    
    }