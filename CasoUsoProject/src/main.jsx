import React from 'react'
import ReactDOM from 'react-dom/client'
import './index.css'
import {createBrowserRouter, RouterProvider} from '../node_modules/react-router-dom'
import '../node_modules/bootstrap/dist/js/bootstrap.bundle.js'
import '../node_modules/bootstrap/dist/css/bootstrap.css'
import UserContextProvider from '../src/context/UserContextProvider.jsx'
import Privatepages from './components/shared/Privatepages.jsx'


import Layout from './pages/layout.jsx'
import Home from './pages/Home.jsx'
import Corsi from './pages/Corsi.jsx'
import Accesso from  './pages/Accesso.jsx'
import Profilo from './pages/Profilo.jsx'
import TotUtenti from './pages/TotUtenti.jsx'
import AdminSpecificUtente from './components/AdminSpecificUtente/AdminSpecificUtente.jsx'
import CreazioneCorso from './pages/CreazioneCorso.jsx'
import UpdateCorso from './pages/UpdateCorso.jsx'


const router = createBrowserRouter([
  {
    element:<UserContextProvider><Layout/></UserContextProvider>,
    children:[
        {
          path:"/",
          element:<Home/>
        },
        {
          path:"/corsi",
          element:<Corsi/>
        },
        {
          path:"/accesso",
          element:<Accesso/>
        },
        {
          path:"",
          element: <Privatepages />,
          children:[
          {
          path:"/profilo",
          element:<Profilo/>
          },
          {
          path:"/allutenti",
          element:<TotUtenti/>
          },
          {
          path:"/creazionecorsoadmin",
          element:<CreazioneCorso/>
          },
          {
            path:"/updatecorsoadmin",
            element:<UpdateCorso/>
            },
           {
          path:"/specificutente",
          element:<AdminSpecificUtente/>
          }
          ]
        }
        
    ]
  }

])

ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
        <RouterProvider router={(router)}>
        </RouterProvider>
    </React.StrictMode>
    
)
