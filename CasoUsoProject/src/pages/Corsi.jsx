import { useEffect, useState } from "react";
import CardCorso from "../components/Card/CardCorso";
import css from '../pages/corsi.module.css'
import Cookies from 'js-cookie'
import { useNavigate } from "react-router-dom";

export default function Corsi() {

    //array di corsi
    const [cardProperties, setCardProperties] = useState([]);

    const navigateTo = useNavigate();

    //rendirizzamento alla pagina per creazione corso
    function handleCreation() {
        navigateTo("/creazionecorsoadmin")
    }

    //controllo ruolo
    function controlloRuoloAdmin(ruoli) {

        let trovato = false;

        if (ruoli === undefined) ruoli = ""

        if (ruoli.includes("admin") || ruoli.includes("Admin")) trovato = true;

        return trovato
    }


    //richiesta get per i dati dei corsi
    async function fetchData() {

        const response = await fetch("http://localhost:8080/corso/corsi/printall", {
            method: "GET",
        })


        const datiCard = await response.json();

        setCardProperties(
            datiCard.map((corso) => ({
                nomeCorso: corso.nomeCorso,
                nomeCategoria: corso.categoria.nome_categoria,
                descrizione_Breve: corso.descrizioneBreve,
                descrizione_Completa: corso.descrizioneCompleta,
                id: corso.id,
                durata: corso.durata,
            }))
        );



    }

    useEffect(() => {

        fetchData()

    }, []);

    return (
        <>
            <div>

                {controlloRuoloAdmin(Cookies.get("ruoli")) ?

                    <div>
                        <div className="mb-5 mt-5 text-center">
                            <strong>Impostazione admin: </strong>
                            <button type="button" className="btn btn-success" data-bs-dismiss="modal" onClick={() => handleCreation()}>
                                Crea corso
                            </button>

                        </div>
                    </div> : null}



                <h1 className="mt-5 mb-5 text-center">Corsi disponibili</h1>

                <div className="container-fluid text-center">

                    {cardProperties.map((CardElement, index) => (

                        <div key={CardElement.id}>

                            <hr className={css.hrText} data-content={++index} />

                            <CardCorso idCorso={CardElement.id} nomeCorso={CardElement.nomeCorso} nomeCategoria={CardElement.nomeCategoria}
                                descrizione_Breve={CardElement.descrizione_Breve} descrizione_Completa={CardElement.descrizione_Completa} durata={CardElement.durata}
                                updateCard={fetchData}>
                            </CardCorso>



                        </div>

                    )
                    )}

                </div>

                {
                    cardProperties.length == 0 ? <div> <h3 className="text-center mt-5">Nessuno corso disponibile</h3><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /></div> : <br />
                }

            </div>

        </>
    )
}