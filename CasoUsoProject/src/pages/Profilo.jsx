import ProfileTable from "../components/ProfileTable/ProfileTable"
import CorsiIscritti from "../components/CorsiUtente/CorsiIscritti"
import css from '../pages/corsi.module.css'

export default function Profilo(){

    return(
        <>
        <div className="container-fluid text-center">
            <ProfileTable></ProfileTable>
            <hr className={`mt-5 ${css.hrText}`} data-content={"Corsi a cui sei iscritto:"} />
                <div className="mt-5">
                    <h1 className="mb-5">Elenco corsi:</h1>
                <CorsiIscritti></CorsiIscritti>
                </div>
        </div>
       
        </>
    )
}