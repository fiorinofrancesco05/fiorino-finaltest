
import { useContext } from "react"
import { UserContext } from "../context/UserContext"
import Login from "../components/login/Login"
import Registration from "../components/registration/Registration"

export default function Accesso(){

  //non sarebbe meglio farlo con i cookie?
    const [userData, setUserData, registrato, setRegistrato] = useContext(UserContext)

    if(registrato == false){

      return(
        <>
        <Registration></Registration>
        </>
      )
    }else{
      
      return(
      <>
      <Login></Login>
      </>
    )
    }
    

}