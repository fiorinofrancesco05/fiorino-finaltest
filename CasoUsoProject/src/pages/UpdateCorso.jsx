import { useState, useContext, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import Cookies from '../../node_modules/js-cookie'

export default function UpdateCorso() {

    const navigateTo = useNavigate();

    //modello form
    const [corsoData, setCorsoData] = useState({
        nome: "",
        desc_breve: "",
        desc_completa: "",
        durata: "",
        categoria:{
            id:"",
            nome_categoria: ""
        }
    })

    //categorie
    const [categorie, setCategorie] = useState([])


    //menage dei dati nel form
    function handleChange(e) {
        const { name, value} = e.target;

        if(name === "categoria"){
            let valori = []
            valori = value.split(",")
            setCorsoData({ ...corsoData, categoria:{ id: valori[0], nome_categoria: valori[1] } }); 
            console.log(corsoData.categoria)
        }else{
            setCorsoData({ ...corsoData, [name]: value }); 
        }

       
    }

    //validazione dati
    function handleSubmit(e) {
        e.preventDefault();
        updateCorso();
    }

    //creaCorso
    async function updateCorso(){

   // let token = Cookies.get("token")

        let idC =  Cookies.get("idCorsoUpdate")
        const corso = {
            id: idC,
            nomeCorso: corsoData.nome,
            descrizioneBreve: corsoData.desc_breve,
            descrizioneCompleta: corsoData.desc_completa,
            durata: corsoData.durata,
            categoria:{
                id: corsoData.categoria.id,
                nome_categoria: corsoData.categoria.nome_categoria
            }
        }

        console.log(corso.nomeCorso)

        const response = await fetch("http://localhost:8080/corso/corsi/updatecorso",{
            method:"POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(corso)
        })

        if(response.status == 200){
            alert("Corso modificato con successo")
            navigateTo("/corsi")
        }else{
            alert("Errore nella modifica")
        }
        
    }

    //get categorie
    async function getCategorie(){

        
        const response = await fetch("http://localhost:8080/corso/categoria",{
            method:"GET"
        })

        const dataCategorie = await response.json();

        setCategorie(
            dataCategorie.map((categoria) => ({
                id: categoria.id,
                nomeCategoria: categoria.nome_categoria
            }))
        );

    }

     //get corso
     async function getCorso(){

        const idCorso = {
            id: Cookies.get("idCorsoUpdate")
        }


        const response = await fetch("http://localhost:8080/corso/corsi/getcorsobyid",{
            headers: {
                'Content-Type': 'application/json',
            },
            method: "POST",
            body: JSON.stringify(idCorso)
        })

        const datiDelCorso = await response.json();

        setCorsoData({
            nome : datiDelCorso.nomeCorso,
            desc_breve : datiDelCorso.descrizioneBreve,
            desc_completa : datiDelCorso.descrizioneCompleta,
            durata : datiDelCorso.durata,
            categoria:{
                id: datiDelCorso.categoria.id,
                nome_categoria: datiDelCorso.categoria.nome_categoria
            }
        }) 

     
        

     }


    useEffect(() => {
        getCategorie()
        getCorso()
    },[])


    return (
        <>
            <div className="container-fluid text-center">

<div className="row mt-5">

    <div className="col-md-4 mt-5"></div>

    <div style={{ border: "1px solid white", borderRadius: "8px" }} className="col-md-4 mt-5 bg-dark">
        <form onSubmit={handleSubmit}>

            <div className="mt-3 text-center">
                <svg style={{ fill: "white" }} xmlns="http://www.w3.org/2000/svg" width="56" height="56" fill="currentColor" class="bi bi-cloud-plus" viewBox="0 0 16 16">
                    <path fill-rule="evenodd" d="M8 5.5a.5.5 0 0 1 .5.5v1.5H10a.5.5 0 0 1 0 1H8.5V10a.5.5 0 0 1-1 0V8.5H6a.5.5 0 0 1 0-1h1.5V6a.5.5 0 0 1 .5-.5" />
                    <path d="M4.406 3.342A5.53 5.53 0 0 1 8 2c2.69 0 4.923 2 5.166 4.579C14.758 6.804 16 8.137 16 9.773 16 11.569 14.502 13 12.687 13H3.781C1.708 13 0 11.366 0 9.318c0-1.763 1.266-3.223 2.942-3.593.143-.863.698-1.723 1.464-2.383m.653.757c-.757.653-1.153 1.44-1.153 2.056v.448l-.445.049C2.064 6.805 1 7.952 1 9.318 1 10.785 2.23 12 3.781 12h8.906C13.98 12 15 10.988 15 9.773c0-1.216-1.02-2.228-2.313-2.228h-.5v-.5C12.188 4.825 10.328 3 8 3a4.53 4.53 0 0 0-2.941 1.1z" />
                </svg>
                <h1 style={{ color: "white" }}>Modifica corso esistente</h1>
            </div>

            <div className="form-floating mb-3 mt-3">
                <input type="text" className="form-control" name="nome" id="floatingInput3" placeholder="Inserire Email" onChange={handleChange} value={corsoData.nome} autoComplete="off" />
                <label htmlFor="floatingInput">Nome corso</label>
            </div>

            <div className="form-floating mb-3">
                <input type="text" name="desc_breve" className="form-control" id="floatingPassword" placeholder="Password" onChange={handleChange} value={corsoData.desc_breve} autoComplete="off" />
                <label htmlFor="floatingPassword">Descrizione breve</label>
            </div>

            <div className="form-floating mb-3">
                <input type="text" name="desc_completa" className="form-control" id="floatingPassword" placeholder="Password" onChange={handleChange} value={corsoData.desc_completa} autoComplete="off" />
                <label htmlFor="floatingPassword">Descrizione completa</label>
            </div>

            <div className="form-floating mb-3">
                <input type="text" name="durata" className="form-control" id="floatingPassword" placeholder="Password" onChange={handleChange} value={corsoData.durata} autoComplete="off" />
                <label htmlFor="floatingPassword">Durata in mesi</label>
            </div>


            <select class="form-select form-select-md mb-3" name="categoria" value={corsoData.categoria.nome} onChange={handleChange} aria-label="Large select example">
                <option  selected>Seleziona categoria corso</option>
                {categorie.map((categoria) => (
                    <option key={categoria.id} value={`${categoria.id},${categoria.nomeCategoria}`} >{categoria.nomeCategoria}</option>
                ))
                }
            </select>



            {/* <select class="form-select form-select-md mb-3" name="" aria-label="Large select example">
                <option selected>Seleziona ruolo</option>
                <option value="1">Studente</option>
                <option value="2">Docente</option>
            </select> */}

            <input type="submit" className="btn btn-success mt-3 mb-2" value="Modifica corso" />
        </form>

    </div>

    <div className="col-md-4"></div>

</div>


</div>
        </>
    )

}