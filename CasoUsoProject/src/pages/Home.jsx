import frontend from "../images/frontend.avif"
import backend from "../images/backend.webp"
import CbScry from "../images/security-1.webp"
import { useNavigate } from "react-router-dom";
import css from "../components/cssLink.module.css"


export default function Home() {

    const navigate = useNavigate()

    function redirect() {
        navigate("/accesso")
    }

    return (
        <>
            <div className="container-fluid text-center">


                {/* titolo */}
                <h1 className="mt-5">Accademy SI gestore Corsi</h1><br></br>

                <div className='text-center'>

                    {/* sottotitolo */}
                    <br /><br /><br /><br />
                    <h4 className='mb-3' >
                        Benvenuto nel nostro portale dedicato alla formazione e allo sviluppo
                        personale e professionale!
                    </h4>
                    <br />
                    <br />

                    {/* hr */}
                    <hr style={{ width: "75%", margin: "auto" }} className='mb-5' />


                    {/* carosello */}
                    <p style={{ fontSize: " 1.3em" }}>
                        Qui troverai un ampio catalogo di corsi accuratamente selezionati
                        per aiutarti a raggiungere i tuoi obiettivi di apprendimento.
                    </p>

                    <div style={{ width: "50%", margin: "auto" }} id="carouselExampleInterval" className="carousel carousel-dark slide" data-bs-ride="carousel">

                        <div className="carousel-inner" style={{ height: "500px" }}>
                            <div className="carousel-item active" data-bs-interval="3000">
                                <img src={frontend} className="w-75 h-100" alt="Frontend" />
                            </div>
                            <div className="carousel-item" data-bs-interval="3000">
                                <img src={backend} className="w-75 h-100" alt="Backend" />
                            </div>
                            <div className="carousel-item" data-bs-interval="3000">
                                <img src={CbScry} className="w-75 h-100" alt="CyberSecurity" />
                            </div>
                        </div>

                        <button className="carousel-control-prev" type="button" data-bs-target="#carouselExampleInterval" data-bs-slide="prev">
                            <span className="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span className="visually-hidden">Previous</span>
                        </button>
                        <button className="carousel-control-next" type="button" data-bs-target="#carouselExampleInterval" data-bs-slide="next">
                            <span className="carousel-control-next-icon" aria-hidden="true"></span>
                            <span className="visually-hidden">Next</span>
                        </button>


                    </div>


                    <div className="text-center mt-5 mb-5 fs-2">Questo ed altro ancora!</div>

                    {/* Link to registrazione */}
                    <br />
                    <div style={{ width: "75%" }} className="container mt-5 mb-5">
                        <p style={{ fontSize: " 1.3em" }}>
                            Registrati ora per accedere a un mondo di opportunità formative e unisciti a una
                            community appassionata di studenti come te, ed inoltre potrai esplorare
                            corsi progettati per soddisfare le tue esigenze specifiche.
                        </p>

                        <span className={css.myLink} onClick={() => redirect()}> Clicca qui per registrarti!</span>
                    </div>

                    {/* hr */}
                    <hr style={{ width: "75%", margin: "auto" }} className='mb-5' />
                    <br></br>

                    <div className="container mt-5">
                        <div className="row">
                            <div className="col-md-1"></div>
                            <div style={{ borderStyle: "inset" }} className="col-md-5">
                                <p className="text-start fw-bold" style={{ fontSize: "2em" }}>Mission:</p>
                                <p className="text-start" style={{ fontSize: "1.3em" }}>
                                    La nostra missione è fornire risorse di apprendimento su misura per te, consentendoti di
                                    acquisire nuove competenze e conoscenze in modo flessibile e accessibile.
                                </p>
                            </div>
                            <div className="col-md-5 d-flex align-items-center justify-content-center fs-2">
                                Investi nel tuo futuro e inizia il tuo viaggio di crescita con noi!
                            </div>



                        </div>
                    </div>



                </div>
                <br /><br /><br />
            </div>


        </>
    )
}