import Navbar from "../components/navbar/Navbar"
import Footer from "../components/Footer/Footer"
import { useOutlet, useNavigate } from "react-router-dom"
import { useContext } from "react"
import { UserContext } from "../context/UserContext"
import Cookies from "js-cookie"
import { useEffect } from "react"

export default function Layout(){

    const refresh = false;

    const [userData, setUserData, registrato, setRegistrato] = useContext(UserContext)

    const outlet = useOutlet()

    const navigate = useNavigate()

    //funzione per pulizia dati successiva al logout
    function handleLogOut() {

            clearInterval(intervalloControllo)
      
            Cookies.remove("nome")
            Cookies.remove("cognome")
            Cookies.remove("email")
            Cookies.remove("ruoli")
            Cookies.remove("emailModificaAdmin")
            Cookies.remove("token")
            Cookies.remove("oldEmail")
    
            setUserData({})
    
        }


    const intervalloControllo = setInterval(checkTTL, 30000);

    function checkTTL(){

            let orarioScadenza = new Date(Cookies.get("ttl"));
        
            let orarioAttuale = new Date();

            if(Cookies.get("ttl") != ""){
            if (orarioScadenza.getTime() <= orarioAttuale.getTime()) {
                
                handleLogOut();
            }
        }
    }

    return(
    <>
    <Navbar></Navbar>
    {outlet}
    <Footer></Footer>
    </>
    )
}